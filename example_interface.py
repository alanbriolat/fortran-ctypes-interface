import sys
import ctypes
from ctypes import POINTER, pointer, byref, Structure
import numpy as np
from numpy import ctypeslib


def VALUE_POINTER(t):
    """Create argument types that automatically convert values to pointers.

    Fortran passes arguments by reference by default, but there's no point in
    juggling pointers if the argument is used read-only.  A
    ``VALUE_POINTER(t)`` argument will accept ``POINTER(t)``, ``t`` or values
    that are valid for ``t(value)``.
    """
    class t_p(POINTER(t)):
        @classmethod
        def from_param(cls, value):
            if isinstance(value, POINTER(t)):
                return value
            elif isinstance(value, t):
                return byref(value)
            else:
                return byref(t(value))
    return t_p


# Type aliases to match with Fortran
f_real = ctypes.c_float
f_integer = ctypes.c_int
f_logical = ctypes.c_bool


class Structure(ctypes.Structure):
    def __repr__(self):
        args = ('%s=%r' % (f, getattr(self, f)) for f, t in self._fields_)
        return '%s(%s)' % (self.__class__.__name__, ', '.join(args))


class ConfigurationType(Structure):
    _fields_ = [('a', f_integer),
                ('b', f_real),
                ('c', f_logical),
                ('d', f_real)]


libname = 'example' if sys.platform == 'win32' else 'libexample'
example = ctypeslib.load_library(libname, '.')

example_subroutine = example.__example_ml_MOD_example_subroutine
example_subroutine.restype = None
example_subroutine.argtypes = [VALUE_POINTER(f_real), POINTER(f_real)]

example_function = example.__example_ml_MOD_example_function
example_function.restype = f_real
example_function.argtypes = [VALUE_POINTER(f_real)]

default_struct = example.__example_ml_MOD_default_struct
default_struct.restype = ConfigurationType
default_struct.argtypes = []

modify_struct = example.__example_ml_MOD_modify_struct
modify_struct.restype = None
modify_struct.argtypes = [POINTER(ConfigurationType)]

a = f_real(12.0)
b = f_real()
example_subroutine(a, byref(b))
print(a, b)
assert b.value == 156.0

c = example_function(12)
print(c)
assert c == 288.0

d = ConfigurationType(a=12)
print(d)
d = default_struct()
print(d)
d.d = 15
print(d)
modify_struct(d)
print(d)
