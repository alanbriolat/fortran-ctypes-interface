module example_ml

    type :: ConfigurationType
        integer :: a = 0
        real :: b = 0.0
        logical :: c = .true.
        real :: d = 0.0
    end type ConfigurationType

    public :: example_subroutine
    public :: example_function
    public :: default_struct
    public :: modify_struct

contains

    subroutine example_subroutine(a, b)
        real, intent(in) :: a
        real, intent(out) :: b

        b = a * a + a
    end subroutine example_subroutine

    function example_function(a) result(b)
        real, intent(in) :: a
        real :: b

        b = (a + a) * a
    end function example_function

    pure function default_struct() result(c)
        type(ConfigurationType) :: c

        c = ConfigurationType()
    end function

    subroutine modify_struct(c)
        type(ConfigurationType), intent(inout) :: c

        c%b = c%d
        c%d = c%d * c%d
        c%c = .true.
    end subroutine modify_struct

end module example_ml
