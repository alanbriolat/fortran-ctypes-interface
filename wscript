#!/usr/bin/env python3

import sys
import os

from waflib.Configure import conf


def options(opt):
    opt.recurse('example')

    opt.parser.set_defaults(prefix='.')


def configure(conf):
    conf.recurse('example')


def build(bld):
    bld.recurse('example')
